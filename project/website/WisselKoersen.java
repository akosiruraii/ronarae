import java.util.Scanner;

public class WisselKoersen {
	
	public static final double dollars = 0.93689;
	public static final double pounds = 0.87718;
	public static final double yen = 122.51492;
	 
	public static void main(String[] args) {
		
			final int kies_dollars = 1;
			final int kies_pounds= 2;
			final int kies_yen = 3;
		 
		Scanner input = new Scanner (System.in);
		System.out.print("Valuta (1 = US dollar, 2 = GB pound, 3 = Yen):  ");
		int kies = input.nextInt();
		
		System.out.print("In te wisselen bedrag (geheele getallen): ");
		int output = input.nextInt();

		System.out.print("\n");
	input.close();
	
		double result = output/ dollars;
			double roundResult = round(result, 2);
		double result2 = output / pounds;
			double roundResult2 = round(result2, 2);
		double result3 = output / yen;
			double roundResult3 = round(result3, 2);
	
		double transactie = roundResult * 0.015;
			double roundTransactie = round(transactie, 2);
		double transactie1 = roundResult2 * 0.015;
			double roundTransactie2 = round(transactie1, 2);
		double transactie2 = roundResult3 * 0.015;
			double roundTransactie3 = round(transactie2, 2);
			
		
					
	if (roundTransactie >= 15 || roundTransactie2 >= 15 || roundTransactie3 >=15 )
		{
		roundTransactie = 15;
		roundTransactie2 = 15 ;
		roundTransactie3 = 15;
		}	
			
	if (roundTransactie <= 2 || roundTransactie2 <= 2 || roundTransactie3 <=2 )
	{
		roundTransactie = 2;
		roundTransactie2 =2 ;
		roundTransactie3 = 2;
	}		
			
		double ontvangen = roundResult - roundTransactie;
		double ontvangen2 = roundResult2 - roundTransactie2;
		double ontvangen3 = roundResult3 - roundTransactie3;
	
		
	if (kies == kies_dollars) {
		System.out.println("Voor " + output + " US dollars krijgt u " + roundResult + " euro.");
		System.out.println("De transactiekosten bedragen " + roundTransactie + " euro.");
		System.out.println("U ontvangt " + ontvangen + " euro.");
		}
	if (kies == kies_pounds){
		System.out.println("Voor " + output + " GB pounds krijgt u " + roundResult2 + " euro.");
		System.out.println("De transactiekosten bedragen " + roundTransactie2 + " euro.");
		System.out.println("U ontvangt " + ontvangen2 + " euro.");
		
	}
	if(kies == kies_yen){
		System.out.println("Voor " + output + " Yen krijgt u " + roundResult3 + " euro.");
		System.out.println("De transactiekosten bedragen " + roundTransactie3 + " euro.");
		System.out.println("U ontvangt " + ontvangen3 + " euro.");
	}
	}
	
	public static double round(double nr, double decimals){
		double multiplier = Math.pow(10, decimals);
		return Math.round(nr* multiplier) / multiplier;
	}
}
