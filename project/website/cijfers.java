import java.util.Scanner;

public class cijfers {
	
	public static void main (String[]args){
		Scanner input = new Scanner(System.in);
		
		System.out.print("Hoeveel cijfers wilt u invoeren? ");
		int aantal = input.nextInt();
		
		double[] cijfers = new double[aantal]; 
		double totaal = 0;
		int voldoendes = 0;
		double max = 0;
		
	
		for (int i = 0; i < cijfers.length; i++){
			System.out.print("Cijfer student " + (i + 1) + ": " );
			cijfers[i] = input.nextDouble();
			if(cijfers[i] >= 5.5){
			voldoendes++;
			}
			
			totaal += cijfers[i];
			max = Math.max(max, cijfers[i]);
			
		}
		input.close();
		
		System.out.println("\nAantal cijfers: " + aantal);
		double gem = totaal / aantal;
			double roundGem = round(gem, 2);
		System.out.println("Gemiddelde cijfer:  " + roundGem);
		System.out.println("Aantal voldoende: " + voldoendes);
		System.out.println("Hoogste cijfer: " + max);
		
	}
	
	public static double round(double nr, double decimals){
		double multiplier = Math.pow(10, decimals);
		return Math.round(nr* multiplier) / multiplier;
	}

}
