
import java.util.Scanner;

public class zoekgetaalreal {
	public static void main(String[] args){
	Scanner input = new Scanner(System.in);
	int lengte;
	
	//loop until input is valid
	do{
		System.out.println("Hoe groot moet het array zijn? (5..25) ");
		lengte = input.nextInt();
	
	}while(lengte < 5 || lengte > 25);
	
	int[] getallen = new int[lengte];
	
	//fill array with random numbers
	for(int i = 0; i < lengte; i++){
		getallen[i] = (int) (Math.random() * 10);
		System.out.print(getallen[i] + " ");
	}
	
	//ask for the 'search number'
	int zoek;
	do{
	System.out.println("\nWelk getal moet ik zoeken? (0..9) ");
	zoek = input.nextInt();
	}while(zoek < 0 || zoek > 9 );
	
	int occ = 0;
	for (int i = 0; i < lengte; i++){
		//if current iteration matches the 'search number'
		if(getallen[i] == zoek){
			occ++;
		}
	input.close();
	}
	
	System.out.println("Het getal " + zoek + " komt " + occ + " keer voor");
	int perc = (int) (((double) occ)/ lengte * 100);
	System.out.println("Dat is " + perc + "%");
	}
	
}

