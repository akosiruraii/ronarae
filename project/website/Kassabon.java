import java.util.Scanner;
public class Kassabon {

    public static void main(String[] args) {

        String[] artikelNaam = {"Papier", "Pennen", "Paperclips", "Potloden", "Gummen"};
        double[] artikelPrijs = {2.5, 1.12, 3.6, 4, 1.89};

        int aantalArtikelen = validateInput("Hoeveel artikelen wilt u kopen?" , 1, 5);

        printArtikelen(artikelNaam, artikelPrijs);

        int[] artikelCode = new int[aantalArtikelen];
        int[] artikelAantal = new int[aantalArtikelen];

        for (int i = 0; i < aantalArtikelen; i++){
            int artikelNr = validateInput("Welk artikel wilt u kopen?", 1, 5);
            System.out.println("Hoeveel " + artikelNaam[artikelNr - 1] + " a " + artikelPrijs[artikelNr - 1] + " wilt u bestellen?");
            Scanner input = new Scanner(System.in);
            artikelAantal[i] = input.nextInt();
            artikelCode[i] = artikelNr;
        }

        double totaalprijs = 0;

        for(int i = 0; i < aantalArtikelen; i++){
            String naam = artikelNaam[artikelCode[i] - 1];
            int aantal = artikelAantal[i];
            double prijs = artikelPrijs[artikelCode[i] - 1];
            double totaal = berekenPrijs(prijs, aantal);

            totaalprijs += totaal;

            System.out.println(aantal + " ex. " + naam + " a " + prijs + " = " + totaal);
        }

        System.out.println("\nTotaalprijs: " + totaalprijs);


    }

    public static int validateInput(String message, int min, int max){
        int answer;
        while (true) {
            Scanner input = new Scanner(System.in);
            try {
                do {
                    System.out.println(message + " (" + min + ".." + max + ")");
                    answer = input.nextInt();
                } while (answer > max || answer < min);
                break;
            } catch (Exception e) {
                System.out.println("Alleen getallen!!!");
            }

        }
        return answer;
    }

    public static void printArtikelen(String[] artikelen, double[] prijzen){
        for (int i = 0; i < artikelen.length; i++) {
            System.out.println((i + 1) + ": " + artikelen[i] + " " + prijzen[i]);
        }
    }

    public static double berekenPrijs(double prijs, int aantal){
        return prijs * aantal;
    }

}
